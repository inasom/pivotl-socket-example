const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// 현재 접속된 유저들의 정보를 저장할 어레이를 선언.
const connectedPeople = [];

io.on('connection', (socket) => {


  // nickname 변수 선언. io의 connection 리스너로 정의된 익명 함수 스코프 내에서만 존재하는 변수이기 때문에 각 socket별로 유니크한 레퍼런스를 가짐에 주의하세요. 이 때문에 socket id 등을 참고할 필요가 없어집니다.
  // 참고 자료: 자바스크립트 스코프와 클로저 https://medium.com/@khwsc1/%EB%B2%88%EC%97%AD-%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8-%EC%8A%A4%EC%BD%94%ED%94%84%EC%99%80-%ED%81%B4%EB%A1%9C%EC%A0%80-javascript-scope-and-closures-8d402c976d19
  let nickname;


  // 해당 소켓에서 메시지를 받으면 nickname과 함께 접속된 유저들에게 전파
  socket.on('chat message', (msg) => {
    io.emit('chat message', {nickname, msg});
  });


  // 소켓에서 닉네임을 받으면 (최초 접속시) 해당 닉네임의 유저가 접속됐음을 이미 접속된 유저들에게 전파하고, 해당 유저에게는 이미 접속되어 있는 유저들의 목록을 전송
  socket.on('nickname', (msg) => {
    nickname = msg;
    io.emit('enter', nickname);
    connectedPeople.forEach((person) => {
      socket.emit('enter', person);
    });
    connectedPeople.push(nickname);
  });


  // 소켓에서 유저가 접속 종료할 경우, 해당 nickname을 접속된 유저 목록에서 삭제하고 접속된 유저들에게 해당 유저가 퇴장했음을 알림.

  socket.on('disconnect', () => {
    console.log('disconnecting: ', nickname);

    // Array의 includes 메소드는 해당 어레이가 특정 밸류를 가진 요소를 포함하는지를 검사하고, 포함할 경우에만 true를 반환합니다. ES 2015에서 추가된 메소드기 때문에 특정 브라우저나 환경에서 작동하지 않을 수 있습니다! 브라우저 단에서 사용할 경우 polyfill을 적용해 주어야 함을 주의하세요.
    if (connectedPeople.includes(nickname)) {
      // splice 메소드의 기능은 MDN 문서를 참고하세요. https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/Array/splice
      connectedPeople.splice(connectedPeople.indexOf(nickname), 1);
    }
    io.emit('exit', nickname);
  });


});

http.listen(3000, () => {
  console.log('listening on *:3000');
});